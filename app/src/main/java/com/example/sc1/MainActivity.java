package com.example.sc1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mBase;
    private EditText mHeight;
    private TextView mHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBase = findViewById(R.id.alas); //getting the alas EditText
        mHeight = findViewById(R.id.tinggi); //getting the tinggi EditText
        mHasil = findViewById(R.id.hasil); //getting the hasil Text View
    }

    public void hitungHasil(View view) { //the method invoked after clicking the button

        String base = mBase.getText().toString(); //storing what's written int the alas EditText
        String height = mHeight.getText().toString(); //storing what's written int the tinggi EditText

        double result = Double.parseDouble(base) * Double.parseDouble(height);
        //result is the result of the multiplication of base and height after being parsed into Double data type

        mHasil.setText(String.valueOf(result)); //result is what is shown in hasil TextView
        mHasil.setVisibility(View.VISIBLE); //now the hasil TextView is Visible


    }
}
